import configparser
import argparse
import os

FILE = 'config-tp1.ini'

parser = argparse.ArgumentParser()
parser.add_argument('--configuration', type=str, required=False)

args = parser.parse_args()
file = args.configuration
config = configparser.ConfigParser()
if (file == None):
    file = os.environ.get('FICHIER_DE_CONFIGURATION')
    if (file == None):
        file = FILE

config.read(file)


print("\n**** All sections & variables : ****")

for section in config.sections():
    print("\nSection: %s" % section)
    for options in config.options(section):
        print("%s --> %s" % (options, config.get(section, options)))
print("\n**********")

