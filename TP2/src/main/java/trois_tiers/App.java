package trois_tiers;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    Metier metier;
    Presentation presentation;
    Stockage stockage;

    
    public void setMetier(Metier metier) {
        this.metier = metier;
    }


    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }


    public void setStockage(Stockage stockage) {
        this.stockage = stockage;
    }


    public Metier getMetier() {
        return metier;
    }


    public Presentation getPresentation() {
        return presentation;
    }


    public Stockage getStockage() {
        return stockage;
    }


    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("trois_tiers/configuration.xml");
        App app = ctx.getBean("app", App.class);
        Metier metier = app.getMetier();
        Stockage stockage = app.getStockage();
        Presentation presentation = app.getPresentation();
        ctx.close();
    }
}
